#include "cave.h"
#include "ui_cave.h"
#include <QFileDialog>





extern int numero_de_carriles;
extern int selecc_persp;
extern char archivo_de_video[200];
extern int bandera_archivo;


extern int cuentedearribaaabajo ;       //Cuenta de abajo a arriba
extern int cuentedeabajoaarriba ;       //Cuenta de arriba a abajo
extern int cuentedeizquierdaaderecha ;  //Cuenta de izquierda a derecha
extern int cuentedederechaaizquierda ;  //Cuenta de derecha a izquierda


cave::cave(QWidget *parent) :
QDialog(parent),
ui(new Ui::cave)
{
	ui->setupUi(this);
	connect(ui->carriles,SIGNAL(valueChanged(int)),this,SLOT(carriles(int)));
	connect(ui->botonperspectivasi,SIGNAL(toggled ( bool )),this,SLOT(ponga_perspectiva(bool)));


	connect(ui->arriba_abajo,SIGNAL(toggled(bool)),this,SLOT(ponga_arriba_abajo(bool)));
	connect(ui->abajo_arriba,SIGNAL(toggled(bool)),this,SLOT(ponga_abajo_arriba(bool)));
	connect(ui->izquierda_derecha,SIGNAL(toggled(bool)),this,SLOT(ponga_izquierda_derecha(bool)));
	connect(ui->derecha_izquierda,SIGNAL(toggled(bool)),this,SLOT(ponga_derecha_izquierda(bool)));



}

cave::~cave()
{
	delete ui;
}

void cave::carriles(const int dato)
{
	numero_de_carriles = dato;
}

void cave::ponga_perspectiva(const bool dato)
{
	if(dato)
	selecc_persp = 1;
	else
	selecc_persp = 0;
}
void cave::abrirvideo(void)
{
	bandera_archivo=1;
	QString file = QFileDialog::getOpenFileName(this,"Selecciona un video",QDir::homePath(),"Videos (*.avi *.mov *.mpg)");
	QByteArray ba = file.toLatin1();
	strcpy(archivo_de_video,ba.data());
}


void cave::ponga_arriba_abajo(bool dato)
{
	if (dato)
	{
		cuentedearribaaabajo =1;       //Cuenta de abajo a arriba
		cuentedeabajoaarriba =0;       //Cuenta de arriba a abajo
		cuentedeizquierdaaderecha =0;  //Cuenta de izquierda a derecha
		cuentedederechaaizquierda =0;  //Cuenta de derecha a izquierda
	}
	else
	{
		cuentedearribaaabajo =0;       //Cuenta de abajo a arriba

	}
}

void cave::ponga_abajo_arriba(bool dato)
{
	if (dato)
	{
		cuentedearribaaabajo =0;       //Cuenta de abajo a arriba
		cuentedeabajoaarriba =1;       //Cuenta de arriba a abajo
		cuentedeizquierdaaderecha =0;  //Cuenta de izquierda a derecha
		cuentedederechaaizquierda =0;  //Cuenta de derecha a izquierda
	}
	else
	{
		cuentedeabajoaarriba =0;       //Cuenta de abajo a arriba

	}
}
void cave::ponga_izquierda_derecha(bool dato)
{
	if (dato)
	{
		cuentedearribaaabajo =0;       //Cuenta de abajo a arriba
		cuentedeabajoaarriba =0;       //Cuenta de arriba a abajo
		cuentedeizquierdaaderecha =1;  //Cuenta de izquierda a derecha
		cuentedederechaaizquierda =0;  //Cuenta de derecha a izquierda
	}
	else
	{
		cuentedeizquierdaaderecha =0;       //Cuenta de abajo a arriba

	}
}
void cave::ponga_derecha_izquierda(bool dato)
{
	if (dato)
	{
		cuentedearribaaabajo =0;       //Cuenta de abajo a arriba
		cuentedeabajoaarriba =0;       //Cuenta de arriba a abajo
		cuentedeizquierdaaderecha =0;  //Cuenta de izquierda a derecha
		cuentedederechaaizquierda =1;  //Cuenta de derecha a izquierda
	}
	else
	{
		cuentedederechaaizquierda =0;       //Cuenta de abajo a arriba

	}
}

