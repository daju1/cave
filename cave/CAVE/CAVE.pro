#-------------------------------------------------
#
# Project created by QtCreator 2010-08-12T17:26:16
#
#-------------------------------------------------

QT       += core gui

TARGET = CAVE
TEMPLATE = app


SOURCES += main.cpp\
        cave.cpp

CONFIG += static
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

FORMS    += cave.ui
win32 {

     message("Hola windows :(")
HEADERS  += cave.h \
    C:\OpenCV-2.4.11/makeqt/include/opencv/highgui.h \
    C:\OpenCV-2.4.11/makeqt/include/opencv/cvaux.h \
    C:\OpenCV-2.4.11/makeqt/include/opencv/cv.h

INCLUDEPATH +=  "C:\OpenCV-2.4.11/makeqt\include\opencv"       \
                "C:\OpenCV-2.4.11/makeqt\src\cvaux"            \
                "C:\OpenCV-2.4.11/makeqt\src\cv"               \
                "C:\OpenCV-2.4.11/makeqt\src\ml"               \
                "C:\OpenCV-2.4.11/makeqt\src\cxcore"           \
                "C:\OpenCV-2.4.11/makeqt\src\highgui"          \
                "C:\OpenCV-2.4.11/makeqt\src"

INCLUDEPATH     += "C:\Qt\Qt5.1.1\5.1.1\mingw48_32\include"
INCLUDEPATH     += "C:\Qt\Qt5.1.1\5.1.1\mingw48_32\include\QtWidgets"

#LIBS += "C:\OpenCV-2.4.11/makeqt\lib\libopencv_cv2411.dll.a"
LIBS += "C:\OpenCV-2.4.11/makeqt\lib\libopencv_*2411.dll.a"
LIBS += "C:\OpenCV-2.4.11/makeqt\lib\libopencv_core2411.dll.a"
LIBS += "C:\OpenCV-2.4.11/makeqt\lib\libopencv_highgui2411.dll.a"
}
linux-g++ {
    message("Hola linux :)")
HEADERS  += cave.h \
    /usr/local/include/opencv/highgui.h \
    /usr/local/include/opencv/cvaux.h \
    /usr/local/include/opencv/cv.h


INCLUDEPATH += /usr/local/include/opencv
HEADERS  += cave.h \
    /usr/local/include/opencv/highgui.h \
    /usr/local/include/opencv/cvaux.h \
    /usr/local/include/opencv/cv.h


INCLUDEPATH += /usr/local/include/opencv
LIBS+=/usr/local/lib/libcxcore.so.2.1.0
LIBS+=/usr/local/lib/libcvaux.so.2.1.0
LIBS+=/usr/local/lib/libhighgui.so.2.1.0
LIBS+=/usr/local/lib/libcv.so.2.1.0
LIBS+=/usr/local/lib/libml.so.2.1.0
LIBS+=/usr/local/lib/libcxcore.so.2.1.0
LIBS+=/usr/local/lib/libcvaux.so.2.1.0
LIBS+=/usr/local/lib/libhighgui.so.2.1.0
LIBS+=/usr/local/lib/libcv.so.2.1.0
LIBS+=/usr/local/lib/libml.so.2.1.0
}
