#ifndef CAVE_H
#define CAVE_H



#include <QDialog>
#include <cv.h>
#include <cvaux.h>
#include <highgui.h>
#include <cxcore.h>
#include <ctype.h>
#include <stdio.h>
#include <math.h>
#include <time.h>


namespace Ui {
	class cave;
}

class cave : public QDialog
{
	Q_OBJECT
	public slots:

	void carriles( int dato);
	void ponga_perspectiva( bool dato);
	void abrirvideo(void);


	void ponga_arriba_abajo(bool dato);
	void ponga_abajo_arriba(bool dato);
	void ponga_izquierda_derecha(bool dato);
	void ponga_derecha_izquierda(bool dato);

public:
	explicit cave(QWidget *parent = 0);
	~cave();

private:
	Ui::cave *ui;
};

#endif // CAVE_H
