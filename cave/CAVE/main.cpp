
/* CONTEO AUTOM�TICO DE VEH�CULOS
Algoritmo realizado por:
Germ�n Enrique Urrego Ni�o
Francisco Carlos Calder�n Bocanegra
bajo licencia Creative commons 2.5
Noviembre 2008



                                              _____________
                                  ..---:::::::-----------. ::::;;.
                               .'""""""                  ;;   \  ":.
                            .''                          ;     \   "\__.
                          .'                            ;;      ;   \\";
                        .'                              ;   _____;   \\/
                      .'                               :; ;"     \ ___:'.
                    .'--...........................    : =   ____:"    \ \
               ..-""                               """'  o"""     ;     ; :
          .--""  .----- ..----...    _.-    --.  ..-"     ;       ;     ; ;
       .""_-     "--""-----'""    _-"        .-""         ;        ;    .-.
    .'  .'                      ."         ."              ;       ;   /. |
   /-./'                      ."          /           _..  ;       ;   ;;;|
  :  ;-.______               /       _________==.    /_  \ ;       ;   ;;;;
  ;  / |      """"""""""".---."""""""          :    /" ". |;       ; _; ;;;
 /"-/  |                /   /                  /   /     ;|;      ;-" | ;';
:-  :   """----______  /   /              ____.   .  ."'. ;;   .-"..T"   .
'. "  ___            "":   '""""""""""""""    .   ; ;    ;; ;." ."   '--"
 ",   __ """  ""---... :- - - - - - - - - ' '  ; ;  ;    ;;"  ."
  /. ;  """---___                             ;  ; ;     ;|.""
 :  ":           """----.    .-------.       ;   ; ;     ;:
  \  '--__               \   \        \     /    | ;     ;;
   '-..   """"---___      :   .______..\ __/..-""|  ;   ; ;
       ""--..       """--"                      .   ". . ;
             ""------...                  ..--""      " :
                        """"""""""""""""""    \        /
                                               "------"


*/
#ifdef HAVE_QT
#include <QApplication>
#include "cave.h"
#endif

#include "opencv2/core/core.hpp"
#include "opencv2/core/internal.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/legacy/legacy.hpp"
#include "opencv2/legacy/compat.hpp"


//#include "cv.h"
//#include "cvaux.h"
//#include "highgui.h"
//#include "cxcore.h"

#include <ctype.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

/*seleccione 1 si quiere grabar 0 si n�*/
#define quierograbar 0               //1 si quiere grabar
#define aprenderfondohist 0          //1 si quiere que se aprenda el fondo midificando histograma
#define actfondosinmov 1             //1 para que se actualize el fondo sin carros
#define despegarblobsgrandes 1       // para que se despeguen los blobs mayores a un tama�o espec�fico
#define momentodelblob 1             // para que use los rect�ngulos mediante momentos de area y no de manera menos ortodoxa
#define dividaclustergrande 1        // para que separe contornos grandes dependiendo de numero_de_carriles
#define mascaradeinteres 1           // Mascara para evitar actualizaci�n de fondo por variaciones externas a la via de interes y para acople de la imagen final.
//compila de arriba a abajo
int cuentedearribaaabajo = 1 ;      //Cuenta de abajo a arriba
int cuentedeabajoaarriba = 0  ;     //Cuenta de arriba a abajo
int cuentedeizquierdaaderecha = 0 ; //Cuenta de izquierda a derecha
int cuentedederechaaizquierda = 0 ; //Cuenta de derecha a izquierda

//Almacenamiento global
//
#if dividaclustergrande
int numero_de_carriles = 3;
#endif
/*bandera para especificar c�mara de video*/
int esdecamara=0;
char archivo_de_video[200];
int bandera_archivo=0;

/*variables globales de petici�n de ROI y perspectiva*/

int add_remove_pt = 0;//bandera que sirve en la funcipon on_mouse para saber cuando se pusieron los dos puntos del ROI
int xl=0, yt=0,w=100,l=100;//variables temporales de ROI
IplImage* Orig_Frame = NULL;//imagen en cuestion
CvPoint pt1,pt2,pt3,pt4; /*+-+-+--+ Variaciones para selecci�n de perspectiva -+-+-+-+-+--+-+-+-+-*/
CvPoint2D32f perspectiva1,perspectiva2,perspectiva3,perspectiva4; /*+-+-+--+ Variaciones para selecci�n de perspectiva -+-+-+-+-+--+-+-+-+-*/
int banderapt1=0,banderapt2=0,finmouse=0,selecc_persp=0,esperadedato=0; /*+-+-+--+ Variaciones para selecci�n de perspectiva -+-+-+-+-+--+-+-+-+-*/
int g_w= 0;
int g_h= 0;
// banderapt1 y banderapt2 sirven para saber cuando se oprimieron los botones izquierdo y derecho
//finmouse dice cuando se seleccionaron los dos puntos de pers...
/*fin de declaraciones de variables globales de petici�n de ROI y perspectiva*/
/*variables globales de fondo por media aumentada*/
IplImage *g_ImMedFlt;
IplImage *g_ImtempFlt;
IplImage *mascaraddiv;
/*variables globales de bordes*/
IplConvKernel * Kernel = cvCreateStructuringElementEx(3 , 3, 1, 1,CV_SHAPE_ELLIPSE,NULL);
IplConvKernel * Kernel2 = cvCreateStructuringElementEx(5 , 5, 2, 2,CV_SHAPE_CROSS,NULL);
IplImage * erodada = NULL;
IplImage * bordesprevios = NULL;
IplImage * threshold = NULL;
IplImage * bordes1canal = NULL;
IplImage * umbral = NULL;
IplImage * logaritmo_t = NULL;
/*fin de variables globales de bordes*/
/*Varables globales para perspectiva*/
CvMat *g_c= cvCreateMat(3, 3, CV_32FC1);
CvMat *g_cinv= cvCreateMat(3, 3, CV_32FC1);
CvPoint2D32f g_src[4],g_dst[4];

int aproxcarril(float R)
{if(1.2<R && R<2.3)
	return 2;
	else if(2.3<=R && R<3.3)
	return 3;
	else if(3.3<=R && R<4.3)
	return 4;
	//else return (int) ceil(R+0.5);
	else
	if (ceil(R-0.3)>=numero_de_carriles)
	return (int)numero_de_carriles;
	else return (int) ceil(R-0.3);
}


/*fin de globales de perspectiva*/
/*funcion para capturar los fps de la c�mara de video*/
double framespersec(CvCapture* lacaptura){
	double timestamp = 0,timestamp2=0;
	int freim=0;
	double duraciondelcuadro=0;
	IplImage* imagensuperimportanteparafps=NULL;
	int N=50;
	cvNamedWindow("imagen_para _calcular_FPS",1);
	for(int frames=0;frames<=N;frames++)
	{
		timestamp = ((double)clock()/CLOCKS_PER_SEC);
		imagensuperimportanteparafps = cvQueryFrame(lacaptura);
		timestamp2 = ((double)clock()/CLOCKS_PER_SEC);
		//printf("la duraci�n del cuadro mas o menos fu�: %f \n",timestamp2-timestamp);
		freim++;
		//timestamp=timestamp/freim;
		//printf("la duraci�n del cuadro mas o menos fu�: %f \n",timestamp);
		duraciondelcuadro+=(timestamp2-timestamp);
		//printf("la duraci�n del cuadro mas o menos fu�: %f \n",duraciondelcuadro);
		cvShowImage("imagen_para _calcular_FPS",imagensuperimportanteparafps);
		//cvWaitKey(0);
	}
	freim--;
	//printf("la diferencia es de : %f \n",duraciondelcuadro);
	//printf("y los frames son: %d \n",freim);
	cvDestroyWindow("imagen_para _calcular_FPS");
	return (freim/(3*duraciondelcuadro));

}//fin de la funcion

/*funciones, y estructuras para seguimiento:*/
static int CompareContorno(const void* a, const void* b, void* )
{
	float           dx,dy;
	float           f_h,f_w,f_ht,f_wt;
	CvPoint2D32f    pa,pb;
	CvRect          ra,rb;
	CvSeq*          pCA = *(CvSeq**)a;
	CvSeq*          pCB = *(CvSeq**)b;
        //int cond_comp_conectados=0;
	ra = ((CvContour*)pCA)->rect;
	rb = ((CvContour*)pCB)->rect;
	pa.x = ra.x + ra.width*0.5f;
	pa.y = ra.y + ra.height*0.5f;
	pb.x = rb.x + rb.width*0.5f;
	pb.y = rb.y + rb.height*0.5f;
	f_w = (ra.width+rb.width)*0.5f;
	f_h = (ra.height+rb.height)*0.5f;

	dx = (float)(fabs(pa.x - pb.x)-f_w);//si dx es negativo, es porque los cuadros est�n unidos en x
	dy = (float)(fabs(pa.y - pb.y)-f_h);//si dy es negativo, es porque los cuadros est�n unidos en y
	//cuanto m�s negativos, m�s cerca ent�n en cada una de las coordenadas
	/*si en ht y wt hay valores mayores a cero, la distancia a la que se deben encontrar los rectangulos puede
		ser mayor, para que los tome como un solo blob
		si son menores a cero, deben estar mas juntos, para que sean tomados como un solo blob
		*/
	//wt = MAX(ra.width,rb.width)*0.1f;//otra descici�n-
	f_wt = -0.2*f_w;
	//ht = MAX(ra.height,rb.height)*0.3f;
	f_ht = -0.2*f_h;
	if(dx < f_wt && dy < f_ht) return 1;//si comparten un pedasito de rectangulo de una retorne que los una
	else
	{

	}
	return 0;//si no encuentra que sean el mismo carro, separelos
}
double error(CvPoint a ,CvPoint b)//por ahora el error es proporcional a la distancia euclideana simple entre dos puntos
{
	return(sqrt(pow((float)(a.x-b.x),2)+pow((float)(a.y-b.y),2)));
}
struct blob {// entiendase como blob a algun elemento de interes de la imagen representado como una mancha blanca en fondo negro que en seguido por algun algoritmo
	int Id; //identificador del blob
	CvPoint pta;//punto donde esta el centro del contorno antes
	CvPoint ptd;//punto donde esta el centro del contorno despu�s
	float dist;//error al anterior blob 0 si es nuevo
	int band;//bandera para saber si esta asignado o no, 1 no asignado 0 si ya
	float velocidad;//velocidad del blob en pixeles por frame px/fr * fr/s = px/s y px/s  m/px = m/s y lista la velocidad!!!
	CvSize tamano;// tama�o del blob
	int bandcontado;//bandera para determinar si un blob es nuevo
};//HAY QUE PONER UNA BANDERA DE QUE UN BLOB TIENE UN SEGUIMIENTO EXITOSO; PONERLA EN la estructura blob y en la funcion nuevoblob

blob nuevoblob(int aid, CvPoint pt1,CvPoint pt0,float dst,int fla,float vel,CvSize tam,int nw)//funcion para crear blobs nuevos
{
	blob nblob={aid,pt1,pt0,dst,fla,vel,tam,nw};//inicializaci�n de blob a retornar
	return nblob;
}
struct registro {//para llevar un temporal de caracteristicas a procesar
	float dist;//error al anterior blob 0 si es nuevo
	int id[2];//0 para i 1 para j en i va el blob y en j
	int flag;//bandera para saber si esta asignado o no 1 no asignado 0 si ya
	CvPoint pt;//punto donde esta el centro del contorno
	CvSize tam;//tama�o del contorno, en ancho y alto (w,h)
};
struct caracteristica_contorno {// caracteristicas a procesar del contorno
	CvRect rectangulo;

};
/*fin de funciones y estructuras para seguimiento*/

//------------------------------------------------------------------------------inicio funciones:
//funciones de petici�n de perspectiva:                                         1
//funciona cuando se mueve algo en la ventana en cuastion
void on_mouse( int event, int x, int y, int flags, void* param )
{
	if(esperadedato==1){
		if( Orig_Frame->origin )//para calibrar el origen del click, con el origen de la imagen
		y = Orig_Frame->height - y;
		if(add_remove_pt==0)
		{
			if( event == CV_EVENT_LBUTTONDOWN )//hallar un punto cuando se baje el click
			{
				pt1 = cvPoint(x,y);
			}
			if( event == CV_EVENT_MOUSEMOVE )//hallar un punto cuando se mueva el click
			{
				pt2 = cvPoint(x,y);
			}
			if( event == CV_EVENT_LBUTTONUP )
			{
				pt2 = cvPoint(x,y);

				if(pt1.x<pt2.x)
				{
					xl=pt1.x;
					w=pt2.x-pt1.x;
				}
				else
				{
					xl = pt2.x;
					w  = pt1.x - pt2.x;
				}
				if(pt1.y<pt2.y)
				{
					yt=pt1.y;
					l=pt2.y-pt1.y;
				}
				else
				{
					yt = pt2.y;
					l  = pt1.y - pt2.y;
				}
				//para corregir tama�o y posibles errores de inicio
				while((w % 4)!= 0 )// si no es par
				{
					w++;
					xl++;
					pt2.x++;
				}
				if((l % 2)!= 0 )// si no es par
				{
					l++;
					yt++;
					pt2.y++;
				}
				add_remove_pt = 1;
			}
		} /*+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ Variaciones para selecci�n de perspectiva -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
		else{
			if(selecc_persp==1){
				if( event == CV_EVENT_LBUTTONDOWN )
				{
					pt4 = cvPoint(x,y);
					if((pt4.x>xl)&&(pt4.x<(xl+w))&&(pt4.y>yt)&&(pt4.y<(yt+l)))
					{
						perspectiva1=cvPointTo32f(pt4);
						//printf("Boton Izquierdo\n");
						banderapt1=1;
					}
				}

				if( event == CV_EVENT_RBUTTONDOWN)
				{
					pt4 = cvPoint(x,y);
					if((pt4.x>xl)&&(pt4.x<(xl+w))&&(pt4.y>yt)&&(pt4.y<(yt+l)))
					{
						perspectiva2=cvPointTo32f(pt4);
						//printf("Boton Derecho\n");
						banderapt2=1;
					}
				}
				if(banderapt1==1 && banderapt2==1)
				finmouse=1;
			}
			else  {
				perspectiva1=cvPointTo32f(cvPoint(MIN(pt1.x,pt2.x),MIN(pt1.y,pt2.y)));
				perspectiva2=cvPointTo32f(cvPoint(MAX(pt1.x,pt2.x),MIN(pt1.y,pt2.y)));
				finmouse=1;
			}
		}  /*+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ Variaciones para selecci�n de perspectiva -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
	}
}
/*Funcion para corregir perspectiva "si se quiere" y seleccionar el ROI*/
//le entra: la imagen a trabajar Orig_Frame definida como global
//le sale:
//        la region de interes, una bandera que dice si hay o no corr de perspectiva
//        ,y 4 CvPoint2D32f TODOS COMO globales
void ROI_y_Perspectiva()
{//inicio funcion RoiYPerspectiva
	IplImage* imgtemp;
	// Crear ventana de la funci�n
	cvNamedWindow("Imagen",1);
	//crea el llamado a control del mouse
	cvSetMouseCallback( "Imagen", on_mouse, 0 );
	int N = Orig_Frame->nChannels;
	for(int i=0;i<Orig_Frame->height;i+=20)
	{// para poner grilla para ayuda del usuario de color verde
		for(int j=0;j<Orig_Frame->width;j+=20)
		{
			CV_IMAGE_ELEM( Orig_Frame, uchar, i, N*(j) + 0 ) = uchar(0);//azul
			CV_IMAGE_ELEM( Orig_Frame, uchar, i, N*(j) + 1 ) = uchar(255);//verde
			CV_IMAGE_ELEM( Orig_Frame, uchar, i, N*(j) + 2 ) = uchar(0);//rojo
		}
	}
	pt3.x=Orig_Frame->width;
	pt3.y=Orig_Frame->height;
	cvShowImage("Imagen",Orig_Frame);


	/*ACA CAMBIO
				case 'P':
						selecc_persp = 1;
						esperadedato=1;
						break;
				case 'n':
						selecc_persp = 0;
						esperadedato=1;

*/
	esperadedato=1;
	printf("2. Seleccionar una region de trabajo, usando el mouse\n");
	for(;;)
	{
		imgtemp = cvCloneImage( Orig_Frame );
		cvRectangle( imgtemp, pt2, pt3, CV_RGB(0,0,100), 1, 8 ,0);
		cvRectangle( imgtemp, pt1, pt2, CV_RGB(0,0,100), 1, 8 ,0);
		cvShowImage("Imagen",imgtemp);
		if( add_remove_pt )
		{
			cvRectangle( Orig_Frame, pt1, pt2, CV_RGB(255,255,255), 3, 8 ,0);
			//printf("Toma tu cuadro \n");
			cvShowImage("Imagen",Orig_Frame);

			/*+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ Variaciones para selecci�n de perspectiva -+-+-+-+-+-+-++-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
		}
		if(finmouse)
		{
			if((perspectiva1.x<xl) ||( perspectiva1.x>(xl+w))||(perspectiva1.y<yt) || (perspectiva1.y>(yt+l))||(perspectiva2.x<xl) || (perspectiva2.x>(xl+w))|| (perspectiva2.y<yt )|| (perspectiva2.y>(yt+l)))
			{
				//printf("Ponga puntos buenos \n");
				finmouse=0;
			}
			else
			{
				//printf("Los puntos son buenos \n");
				cvRectangle( imgtemp, pt1, pt2, CV_RGB(255,255,255), 3, 8 ,0);
				cvCircle(imgtemp,cvPointFrom32f(perspectiva1),3, CV_RGB(255,0,0), -1, 8,0);
				cvCircle(imgtemp,cvPointFrom32f(perspectiva2),3, CV_RGB(0,255,255), -1, 8,0);
				cvLine(imgtemp,cvPointFrom32f(perspectiva1),cvPoint(MIN(pt1.x,pt2.x),MAX(pt1.y,pt2.y)),CV_RGB(255,255,0),2, CV_AA, 0 );
				cvLine(imgtemp,cvPointFrom32f(perspectiva2),cvPoint(MAX(pt1.x,pt2.x),MAX(pt1.y,pt2.y)),CV_RGB(255,255,0),2, CV_AA, 0 );
				cvShowImage("Imagen",imgtemp);
			}
                        char m=(char)cvWaitKey(2)&0xff;
			if(m=='p'||m=='P'||selecc_persp==0)
			break;
			/*+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ Variaciones para selecci�n de perspectiva -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
		}
		if( cvWaitKey(1) >= 0 )
		{
			printf("salio de captura de ROI\n");
			break;
		}
		cvReleaseImage(&imgtemp);
	}
}//fin funcion RoiYPerspectiva
void CrearMatricesDePerspectiva()
{
	//cvWarpPerspectiveQMatrix(g_dst, g_dst, g_c);
	cvWarpPerspectiveQMatrix(g_src, g_dst, g_c);
	cvWarpPerspectiveQMatrix(g_dst, g_src, g_cinv);
}
void PuntosDePerspectiva()
{
	perspectiva3.x=perspectiva1.x-xl;
	perspectiva3.y=perspectiva1.y-yt;
	perspectiva4.x=perspectiva2.x-xl;
	perspectiva4.y=perspectiva2.y-yt;

	/*
		g_src[0]=cvPoint2D32f(0,0);
		g_src[1]=cvPoint2D32f(g_w,0);
		g_src[2]=perspectiva4;
		g_src[3]=perspectiva3;
		g_dst[0]=cvPoint2D32f(0,0);
		g_dst[1]=cvPoint2D32f(g_w,0);
		g_dst[2]=cvPoint2D32f(g_w,g_h);
		g_dst[3]=cvPoint2D32f(0,g_h);
*/

	g_src[0]=cvPoint2D32f(g_w,g_h);
	g_src[1]=cvPoint2D32f(0,g_h);
	g_src[2]=perspectiva3;
	g_src[3]=perspectiva4;
	g_dst[0]=cvPoint2D32f(g_w,g_h);
	g_dst[1]=cvPoint2D32f(0,g_h);
	g_dst[2]=cvPoint2D32f(0,0);
	g_dst[3]=cvPoint2D32f(g_w,0);
}
void Ubicar_Imagenes( IplImage* I ){
	CvSize sz = cvGetSize( I );
	g_ImMedFlt     = cvCreateImage( sz, IPL_DEPTH_32F, 3 );
	mascaraddiv= cvCreateImage( sz, IPL_DEPTH_32F, 1 );
	for( int y=0; y<mascaraddiv->height; y++ ) {
		float* masc = (float*) (mascaraddiv->imageData + y * mascaraddiv->widthStep);//Apuntador de la imagen a sumar
		for( int x=0; x<mascaraddiv->width; x++ ) {
			masc[x]=0.00001;
		}
	}//rellena la imagen con un valor peque, para evitar div por cero
	cvZero( g_ImMedFlt );
	g_ImtempFlt  = cvCreateImage( sz, IPL_DEPTH_32F, 3 );
	cvZero( g_ImtempFlt );
}

//le entran la imagen a ser sumada, las cotas superiores e inferiores, la dispersi�n del color, y el porcentaje
//de aceptaci�n de un nuevo fondo diferente de gris, preferiblemente menor a 0.5 para que trabaje
//
void EstimacionMediaCondPix( IplImage* imgF,float CotaInf,float CotaSup,float Dispersion,float pesobuenos) {
	for( int y=0; y<imgF->height; y++ ) {
		float* ptrimg = (float*) (imgF->imageData + y * imgF->widthStep);//Apuntador de la imagen a sumar
		float* ptravg = (float*) (g_ImMedFlt->imageData + y * g_ImMedFlt->widthStep);//Apuntador de la imagen a sumar
		float* masc = (float*) (mascaraddiv->imageData + y * mascaraddiv->widthStep);//Apuntador de la imagen a sumar
		for( int x=0; x<imgF->width; x++ ) {
			if(
					((ptrimg[3*x+0] > CotaInf && ptrimg[3*x+0] < CotaSup)
						&& (ptrimg[3*x+1] > CotaInf && ptrimg[3*x+1] < CotaSup)
						&& (ptrimg[3*x+2] > CotaInf && ptrimg[3*x+2] < CotaSup))
					/*otra*/ &&    ((fabs(ptrimg[3*x+0]-ptrimg[3*x+1])<Dispersion)
						&&     (fabs(ptrimg[3*x+1]-ptrimg[3*x+2])<Dispersion)
						&&     (fabs(ptrimg[3*x+0]-ptrimg[3*x+2])<Dispersion))
					)
			{
				masc[x]+=pesobuenos;//si entra ac�, sumele el peso correspondiente a la mascara
				ptravg[3*x+0]=(((ptravg[3*x+0]+pesobuenos*ptrimg[3*x+0])));
				ptravg[3*x+1]=(((ptravg[3*x+1]+pesobuenos*ptrimg[3*x+1])));
				ptravg[3*x+2]=(((ptravg[3*x+2]+pesobuenos*ptrimg[3*x+2])));
			}
			else {
				masc[x]++;//si entra ac� solo sume uno a la mascara
				ptravg[3*x+0]=ptravg[3*x+0]+ptrimg[3*x+0];
				ptravg[3*x+1]=ptravg[3*x+1]+ptrimg[3*x+1];
				ptravg[3*x+2]=ptravg[3*x+2]+ptrimg[3*x+2];
			}
		}
	}
}


// Learn the background statistics for one more frame
// I is a color sample of the background, 3-channel, 8u
//
void AcumuleNuevaImagenAlFondo( IplImage *I ){
	cvCvtScale(  I, g_ImtempFlt, 1, 0 );     // convierte a float
	EstimacionMediaCondPix( g_ImtempFlt,30.0,170.0,20.0,5);//( g_ImtempFlt,30.0,170.0,20.0,5);
}

void CalculeFondoAcumulado() {
	for( int y=0; y<mascaraddiv->height; y++ ) {
		float* masc = (float*) (mascaraddiv->imageData + y * mascaraddiv->widthStep);//Apuntador de la imagen a sumar
		float* ptravg = (float*) (g_ImMedFlt->imageData + y * g_ImMedFlt->widthStep);//Apuntador de la imagen a sumar
		for( int x=0; x<mascaraddiv->width; x++ ) {
			ptravg[3*x+0]=ptravg[3*x+0]/masc[x];
			ptravg[3*x+1]=ptravg[3*x+1]/masc[x];
			ptravg[3*x+2]=ptravg[3*x+2]/masc[x];
		}
	}
}

void Des_Ubicar_Imagenes()
{
	cvReleaseImage( &g_ImMedFlt);
	cvReleaseImage( &mascaraddiv );
	cvReleaseImage( &g_ImtempFlt );
}


IplImage* fondo_media_aumentada_roi_perspectiva(CvCapture* captura,CvRect regdeint,int muestreproceso)
{
	cvNamedWindow("Video sin roi", 1);
	cvNamedWindow("Video", 1);
	cvNamedWindow("fondo definitivo", 1);
	int cuadros =30*5;//digamos 30 FPS por 5 Segundos
	if(!esdecamara)
	cuadros =  ceil(((int)cvGetCaptureProperty( captura, CV_CAP_PROP_FRAME_COUNT)-1)/10);//digamos una decima parte del video para entrenar
	int temporalpp = 0;

	IplImage * tempdiv =0;
	IplImage *imagensinroi = NULL;
	imagensinroi = cvQueryFrame( captura );
	cvShowImage("Video sin roi", imagensinroi);
	/* -*-*-*-*-*-*-Ac� va la parte de variaci�n de roi-*-*-*-*-*-*-*-*-*/
	cvSetImageROI(imagensinroi,regdeint)  ;//Selecciona una regi�n de inter�s
	IplImage *imagensinpersp = cvCreateImage(cvSize(regdeint.width,regdeint.height),8,3);//Crea una imagen con el tama�o de ROI
	//imagensinpersp->origin=imagensinroi->origin;
	cvCopy(imagensinroi,imagensinpersp); //Copia la Informaci�n de RDI
	/*-*-*-*fion de la variaci�n del roi-*-*-*-**/
	/*imagen definitiva queda en:*/
	IplImage *imagen = cvCreateImage(cvSize(regdeint.width,regdeint.height),8,3);//Crea una imagen con el tama�o de ROI
	//imagen->origin=imagensinroi->origin;
	/*inicio de la variaci�n de perspectiva*/

	//cvWarpPerspective(imagensinpersp, imagen, g_c, CV_INTER_CUBIC,cvScalar(255,100,255));
	cvCopy(imagensinpersp,imagen);
	cvShowImage("Video", imagen);
        char key_pressed;
	Ubicar_Imagenes(imagen);
	IplImage* tempBG  = cvCreateImage( cvGetSize(imagen), 8, 3 );//para ver el resultado del fondo
	//tempBG->origin=imagen->origin;
	IplImage * div = cvCreateImage( cvGetSize(imagen), 8, 3 ); //para ver en timepo real el resultado del fondo;
	//div->origin=imagen->origin;
	IplImage * restaim = cvCreateImage( cvGetSize(imagen), 8, 3 ); //para estimar la cantidad de movimiento
	//restaim->origin=imagen->origin;
	IplImage * imanterior = cvCreateImage( cvGetSize(imagen), 8, 3 ); //para estimar la cantidad de movimiento
	//imanterior->origin=imagen->origin;
	cvCopy(imagen,imanterior);
	if(muestreproceso)
	{
		cvNamedWindow("fondo estimado actual", 1);
		/*1.  imagen para ver el estado actual del fondo*/
		tempdiv = cvCreateImage( cvGetSize(imagen), IPL_DEPTH_32F, 3 );//para ver en timepo real el resultado del fondo;
		//tempdiv->origin=imagen->origin;
		/*1.  fin declaraci�n imagen para ver el estado actual del fondo*/
	}
	for(temporalpp=0;temporalpp<cuadros;temporalpp++)
	{//Fin del procesamiento del video
		if(imagen == NULL)break;
		if(!esdecamara)
		cvSetCaptureProperty( captura, CV_CAP_PROP_POS_FRAMES, temporalpp);
		imagensinroi = cvQueryFrame( captura );
		cvSetImageROI(imagensinroi,regdeint)  ;//Selecciona una regi�n de inter�s
		cvCopy(imagensinroi,imagensinpersp); //Copia la Informaci�n de RDI
		/*-*-*-*fion de la variaci�n del roi-*-*-*-**/
		/*inicio de la variaci�n de perspectiva*/
		//cvWarpPerspective(imagensinpersp, imagen, g_c, CV_INTER_CUBIC,cvScalar(255,100,255));
		cvCopy(imagensinpersp,imagen);
		cvShowImage("Video", imagen);
		cvAbsDiff(imagen, imanterior,restaim);
		cvThreshold(restaim,restaim,50,1,CV_THRESH_BINARY);//los valores de umbral de 50 y de cantidad de movimiento en 0.005 fueron hallados mediante experimentaci�n
		double count = cvNorm( restaim, 0, CV_L1, 0 ); // acumula los unos en la silueta
		if( count > (restaim->width*imagen->height * 0.005) )//si el movimiento total es mayor al 0.05% de el tama�o del la imagen , guarde fondo
		{
			AcumuleNuevaImagenAlFondo(imagen);//acumula la imagen a la media aumentada
		}
		if(muestreproceso)
		{
			/*1.  Para ver el estimado actual del fondo*/
			for( int y=0; y<mascaraddiv->height; y++ )
			{
				float* ptmpdiv =(float*) (tempdiv->imageData + y * tempdiv->widthStep);//apuntador a la imagen destino
				float* masc = (float*) (mascaraddiv->imageData + y * mascaraddiv->widthStep);//Apuntador de la imagen con la mascara de division
				float* ptravg = (float*) (g_ImMedFlt->imageData + y * g_ImMedFlt->widthStep);//Apuntador de la imagen con el acumulado
				for( int x=0; x<mascaraddiv->width; x++ )
				{
					ptmpdiv[3*x+0]=ptravg[3*x+0]/masc[x];
					ptmpdiv[3*x+1]=ptravg[3*x+1]/masc[x];
					ptmpdiv[3*x+2]=ptravg[3*x+2]/masc[x];
				}
			}
			/*1.  Para ver el estimado actual del fondo*/
			cvConvert(tempdiv,div);
			cvShowImage("fondo estimado actual", div);
			/*comente lo anterior si no quiere ver el estimado actual del fondo*/
		}
		cvCopy(imagen,imanterior);

                key_pressed = ((char)cvWaitKey(2)&0xff);
		if (temporalpp>=cuadros+1 ||temporalpp< 0)break;
                if ((key_pressed) == 27 ) break;//sale en Esc
	}//video
	CalculeFondoAcumulado();

	IplImage *fondodelvideo=cvCreateImage(cvGetSize(imagen),8,3);
	//        fondodelvideo->origin=imagen->origin;
	cvConvert(g_ImMedFlt,fondodelvideo);
	cvShowImage("fondo definitivo",fondodelvideo);
	if(muestreproceso)/*otro pedazo de c�digo para debug no mas*/
	cvDestroyWindow("fondo estimado actual");
	cvDestroyWindow("Video sin roi");
	Des_Ubicar_Imagenes();//borra todo lo dem�s que ya no queremos
	return(fondodelvideo);
}

void CrearTamImBordes(IplImage* I)
{
	/*llenado de variables de bordes*/
	erodada = cvCreateImage(cvSize(I->width, I->height), I->depth ,I->nChannels);
	//        erodada->origin=I->origin;
	bordesprevios = cvCreateImage(cvSize(I->width,I->height),I->depth,I->nChannels);
	//        bordesprevios->origin=I->origin;
	bordes1canal = cvCreateImage(cvSize(I->width,I->height),I->depth,1);
	//        bordes1canal->origin=I->origin;
	umbral = cvCreateImage(cvSize(I->width,I->height),I->depth,1);
	//        umbral->origin=I->origin;
	logaritmo_t = cvCreateImage(cvSize(I->width,I->height), IPL_DEPTH_32F,1);
	//        logaritmo_t->origin=I->origin;
	/*fin de llenado de variables globales de bordes*/
}



void BordesMorfologia(IplImage* I,IplImage* bordesfinal,int esfondo)
{
	/*inicio algoritmo de bordes*/
	cvErode(I,erodada,Kernel,1);//erodacion que se resta a la imagen original
	cvSub(I, erodada, bordesprevios,0); //Se tienen los bordes del Fondo//falta aumentarlos y hacerlos binarios
	cvCvtColor(bordesprevios, bordes1canal, CV_RGB2GRAY );//se convirete a un solo canal
	cvPow(bordes1canal, bordes1canal,2);//se eleva al cuadrado y se saca logaritmo para aumentar escala
	cvConvertScale(bordes1canal,logaritmo_t,(1/93.8092),0);
	cvLog(logaritmo_t, logaritmo_t);
	cvConvertScale(logaritmo_t,bordes1canal,255.0,0);

	if(esfondo)
	{/*para fondo hagale esto a la imagen*/
		//cvThreshold( bordes1canal, umbral, 125,255, CV_THRESH_BINARY_INV);
		cvAdaptiveThreshold( bordes1canal, umbral,255,CV_ADAPTIVE_THRESH_MEAN_C,CV_THRESH_BINARY,3,10 );
		cvErode(umbral,umbral,Kernel2,2);
		cvCvtColor(I, bordes1canal, CV_RGB2GRAY );//se convirete a un solo canal
		cvCanny( bordes1canal, bordes1canal, 50,30, 3 );//solo en este paso, haga los bordes a la imagen en blanco y negro
		cvNot(bordes1canal,bordes1canal);
		cvErode(bordes1canal,bordes1canal,Kernel,2);
		cvAnd(bordes1canal,umbral,bordesfinal);

	}/*fin de bordes del fondo*/
	else
	{/*para fondo de frame del video hace eso otro*/
		//cvThreshold( bordes1canal, bordesfinal, 125,255, CV_THRESH_BINARY);
		cvAdaptiveThreshold( bordes1canal, bordesfinal,255,CV_ADAPTIVE_THRESH_MEAN_C,CV_THRESH_BINARY_INV,3,10 );
	}/*para fondo de frame del video*/
}


template <class T> T **Alloc2DMat(size_t size1, size_t size2)
{
	T **v;
	size_t k;
	v = new T*[size1];
	v[0] = new T[size1 * size2];

	for (k = 1; k < size1; k++)
		v[k] = v[k - 1] + size2;
	return (v);
}

template <class T> void Free2DMat(T ** v)
{
	if (v[0])
		delete[] v[0];
	if (v)
		delete[] v;
}


/*
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    M
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*     A
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-      I
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*       N
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*/

int main(int argc, char *argv[])
{
#ifdef HAVE_QT
	QApplication a(argc, argv);
	cave aplicacionqt;
	aplicacionqt.show();
	a.exec();
#endif

	/*INICIO OPENCV*/


	/*variables para grabar video*/
#if quierograbar
	CvVideoWriter* creavideo=NULL;
	/*fin de variables para hacer video*/
#endif


	/*variables para seguimiento:*/
	//variables a declarar para seguimiento:
	int contdef=0;
	const int TAM=200;//tama�o de la matriz temporal de contornos podemos segir 200 objetos
	//registro regi[TAM][TAM];
	registro** regi = Alloc2DMat<registro>(TAM, TAM);

	int i=0;
	int j=0;
	int fg=1;//bandera de primera vez que se encuentra un contorno debe estar en uno al comienzo
	int aidi=1;
	//para contornos
	CvMemStorage* storage = cvCreateMemStorage(0);//storage para contornos y clasters
	CvMemStorage* stgclaster = cvCreateMemStorage(0);//storage para contornos y clasters
	CvSeq* contour = 0;
	//CvSeq* cntmp=0;
	CvRect bndRect = cvRect(0,0,0,0);
	CvPoint sq1,sq2,tpv,tpt,pcont;
	double perimeter=0.0f;
	double Maxperimeter=130.0f;//que en un fururo sea un #define para establecer el perimeto minimo a seguir
	double MAXAREA=750.0f;//que en un fururo sea un #define para establecer el area minima a seguir
	double MAX_DISTANCIA_DE_ASIGNACION=40.0f;//maxima distancia para que un blob sea considerado el mismo


	//blobs seguimiento:
	/*DECLARACI�N DE UNA SECUENCIA DE BLOBS USANDO CVSEQ Y LA ESTRUCTURA BLOBS DE PREFERENCIA*/
	CvMemStorage* almacenamiento_de_blobs = cvCreateMemStorage(0);
	CvSeq* blobs = cvCreateSeq( 0, /* HAGA UNA SECUENCIA GENERICA */
	sizeof(CvSeq), /* SIEMPRE VA AC� ??? */
	sizeof(blob), /* TAMA�O DE LA SECUENCIA */
	almacenamiento_de_blobs /* EL CvMemStorage DE ALMACENAMIENTO */ );
	/*DECLARACI�N DE UNA SECUENCIA DE CONTORNOS USANDO CVSEQ Y LA ESTRUCTURA almacenamiento_de_contornos DE PREFERENCIA*/
	CvMemStorage* almacenamiento_de_contornos = cvCreateMemStorage(0);
	CvSeq* contornos_nuevos = cvCreateSeq( 0, /* HAGA UNA SECUENCIA GENERICA */
	sizeof(CvSeq), /* SIEMPRE VA AC� ??? */
	sizeof(caracteristica_contorno), /* TAMA�O DE LA SECUENCIA */
	almacenamiento_de_contornos /* EL CvMemStorage DE ALMACENAMIENTO */ );

	//para clasificaci�n
	CvSeq*          cnt_list = cvCreateSeq(0,sizeof(CvSeq),sizeof(CvSeq*), stgclaster );
	CvSeq*          clasters = NULL;
	int             claster_cur, claster_num;

	//GUI:
	CvFont font,defin;
	char conteodef[5];
	char numeros[20];
	/*fin de variables para seguimiento*/

	/*para captura desde c�mara de video*/

	CvCapture *capture;
	if (bandera_archivo)
	{
		//printf (archivo_de_video);
		capture = cvCaptureFromAVI(archivo_de_video);
	}
	else if( argc == 1 || (argc == 2 && strlen(argv[1]) == 1 && isdigit(argv[1][0]))){
		capture = cvCaptureFromCAM( argc == 2 ? argv[1][0] - '0' : 0 );
		esdecamara=1;
	}
	else if( argc == 2 )
	capture = cvCaptureFromAVI( argv[1] );

	if( !capture )
	{
		fprintf(stderr,"No se pudo iniciar.\n");
		return -1;
	}
	CvCapture *copia1decaptura=capture;
	/*petici�n de ROI y perspectiva*/
	pt1.x=0;
	pt1.y=0;
	Orig_Frame = cvQueryFrame(copia1decaptura);
	if(!Orig_Frame)
	{
		printf("mal video saliendo... oprima una tecla para continuar \n");
		//cvWaitKey(0);
		return -1;
	}
	else
	{
		//printf("buen video iniciando..... \n");
		ROI_y_Perspectiva();
		//printf("Salio de la captura de ROI y Perspectiva\n");


		/* ACA CAMBIO
						numero_de_carriles = 2;
						selecccarril=1;
*/

		//cvWaitKey(0);
	}

	/*fin de petici�n de ROI y de perspectiva*/
	g_w = w-1;
	g_h = l-1;
	PuntosDePerspectiva();

	IplImage *fondo=0;
	CvRect regideinte={xl,yt,w,l};

	CrearMatricesDePerspectiva();

	fondo=fondo_media_aumentada_roi_perspectiva(copia1decaptura,regideinte,1);//le entra copia de la captura actual, una region de inter�s y 1 o 0 si se quiere debugging o no
	if(fondo==0)
	cvShowImage("fondo definitivo",fondo);


	//printf(" se llego a la primera parte del algoritmo con exito \n oprima una tecla para continuar...\n");
	//cvWaitKey(0);
	/* en este punto ya tenemos el fondo en la imagen fondo, lo que resta en sacarle
bordes y recorrer el video, sacarle bordes a cada imagen y restar estos dos
�ltimos, con estos hacer la or en el programa definitivo con el FG de BG/FG
*/
	/*  hallarle los bordes al fondo*/
	CrearTamImBordes(fondo);
	IplImage * bordesfondo_inv = cvCreateImage(cvSize(fondo->width,fondo->height),8,1);
	//        bordesfondo_inv->origin=fondo->origin;

	BordesMorfologia(fondo,bordesfondo_inv,1);//1 para fondo 0 para primer plano
	int Ncuadros =1000000;
	if(!esdecamara)
	{
		cvSetCaptureProperty( capture, CV_CAP_PROP_POS_FRAMES, 0);//para corregir bug de que no queda el apuntador al inicio
		Ncuadros =  (int)cvGetCaptureProperty( capture, CV_CAP_PROP_FRAME_COUNT)-1;
	}
	IplImage *imagensinroi=NULL;
	IplImage *image = cvCreateImage(cvSize(regideinte.width,regideinte.height),8,3);//Crea una imagen con el tama�o de ROI
	//        image->origin=fondo->origin;
	IplImage *imagensinpersp = cvCreateImage(cvSize(regideinte.width,regideinte.height),8,3);
	//        imagensinpersp->origin=fondo->origin;


	imagensinroi = cvQueryFrame( capture );
	cvSetImageROI(imagensinroi,regideinte);
	cvCopy(imagensinroi,imagensinpersp);

	//cvWarpPerspective(imagensinpersp, imagen, g_c, CV_INTER_CUBIC,cvScalar(255,100,255));
	cvCopy(imagensinpersp,image);
	/*parametros e inicializada de fondo*/
	CvFGDStatModelParams* params = new CvFGDStatModelParams;
	params->Lc= 128;                  /* Quantized levels per 'color' component. Power of two, typically 32, 64 or 128.                               */
	params->N1c=15;                 /* Number of color vectors used to model normal background color variation at a given pixel.  (15)                  */
	params->N2c=25;                 /* Number of color vectors retained at given pixel.  Must be > N1c, typically ~ 5/3 of N1c.    (25)                 */
	/* Used to allow the first N1c vectors to adapt over time to changing background.                               */

	params->Lcc=64;                 /* Quantized levels per 'color co-occurrence' component.  Power of two, typically 16, 32 or 64.                 */
	params->N1cc=25;                /* Number of color co-occurrence vectors used to model normal background color variation at a given pixel. (25)     */
	params->N2cc=40;                /* Number of color co-occurrence vectors retained at given pixel.  Must be > N1cc, typically ~ 5/3 of N1cc. (40)    */
	/* Used to allow the first N1cc vectors to adapt over time to changing background.                              */

	params->is_obj_without_holes=TRUE ;/* If TRUE we ignore holes within foreground blobs. Defaults to TRUE.                                           */
	params->perform_morphing=2;    /* Number of erode-dilate-erode foreground-blob cleanup iterations.                                             */
	/* These erase one-pixel junk blobs and merge almost-touching blobs. Default value is 1.                        */

	params->alpha1=0.01;              /* How quickly we forget old background pixel values seen.  Typically set to 0.1                                */
	params->alpha2=0.0005;              /* "Controls speed of feature learning". Depends on T. Typical value circa 0.005.                               */
	params->alpha3=0.1;              /* Alternate to alpha2, used (e.g.) for quicker initial convergence. Typical value 0.1.                         */

	params->delta=2;               /* Affects color and color co-occurrence quantization, typically set to 2.                                      */
	params->T=0.9;                   /* "A percentage value which determines when new features can be recognized as new background." (Typically 0.9).*/
	params->minArea=200;             /* Discard foreground blobs whose bounding box is smaller than this threshold.           */
	/*fin de parametros de fondo*/
	CvBGStatModel* bgModel = cvCreateFGDStatModel(imagensinpersp,params); //Crea el StatModel con la imagen despu�s de corregir perspectiva
	//	/*aprendizaje previo del fondo*/
	//	for (int pepe=0;pepe<2;pepe++)
	//	cvUpdateBGStatModel( fondo, bgModel );

	/*aprendizaje de fondo por compensaci�n de histograma*/
#if aprenderfondohist
	double fondingpercent =0.1;
	IplImage* fondotemp = cvCreateImage(cvSize(fondo->width+((int)(fondingpercent*fondo->width)),fondo->height), 8, 3 );
	//        fondotemp->origin=fondo->origin;
	IplImage* fondocompensao = cvCreateImage(cvSize(fondo->width,fondo->height), 8, 3 );
	//        fondocompensao->origin=fondo->origin;
	IplImage* tempuncanal = cvCreateImage(cvSize(fondo->width+((int)(fondingpercent*fondo->width)),fondo->height), 8, 1 );
	//        tempuncanal->origin=fondo->origin;
	cvNamedWindow("Compensation of the fonding",1);

	for(int ja=0;ja<=256;ja+=16)
	{
		//printf("jajaja esta en %d %d %d \n",ja,ja,ja);
		cvSetImageROI(fondotemp,cvRect(0,0,fondo->width,fondo->height));
		cvCopy(fondo,fondotemp);
		cvSetImageROI(fondotemp,cvRect(fondo->width,0,((int)(fondingpercent*fondo->width)),fondo->height));
		cvSet(fondotemp,cvScalar(ja,ja,ja));
		cvResetImageROI(fondotemp);
		//cvShowImage("Compensation of the fonding",fondotemp);
		cvSetImageCOI(fondotemp,1);
		cvCopy(fondotemp,tempuncanal);
		cvEqualizeHist( tempuncanal,tempuncanal);
		cvCopy(tempuncanal,fondotemp);
		cvSetImageCOI(fondotemp,2);
		cvCopy(fondotemp,tempuncanal);
		cvEqualizeHist( tempuncanal,tempuncanal);
		cvCopy(tempuncanal,fondotemp);
		cvSetImageCOI(fondotemp,3);
		cvCopy(fondotemp,tempuncanal);
		cvEqualizeHist( tempuncanal,tempuncanal);
		cvCopy(tempuncanal,fondotemp);

		cvSetImageCOI(fondotemp,0);
		cvShowImage("Compensation of the fonding",fondotemp);
		cvSetImageROI(fondotemp,cvRect(0,0,fondo->width,fondo->height));
		cvCopy(fondotemp,fondocompensao);
		//cvShowImage("Compensation of the fonding",fondocompensao);
		//cvUpdateBGStatModel( fondo, bgModel );
		cvUpdateBGStatModel( fondocompensao, bgModel );
		//cvWaitKey(0);
	}
	cvDestroyWindow("Compensation of the fonding");
#endif

	//	cvNamedWindow("FG",1);
	//	cvNamedWindow("salida con bordes",1);
	//	cvNamedWindow("Bordes Fondo",1);
	//	cvShowImage("Bordes Fondo",bordesfondo_inv);
	//	cvNamedWindow("Bordes Imagen", 1);
	//	cvNamedWindow("Diferencia de Bordes", 1);

	IplImage * restaimg = cvCreateImage( cvGetSize(image), 8, 3 );
	//        restaimg->origin=image->origin;
	IplImage * tempFG = cvCreateImage( cvGetSize(image), 8, 1 );
	//        tempFG->origin=image->origin;
	IplImage* diferbord = cvCreateImage(cvSize(fondo->width,fondo->height),fondo->depth,1);
	//        diferbord->origin=fondo->origin;
	IplImage * bordesframe = cvCreateImage(cvSize(regideinte.width,regideinte.height),8,1);//Crea una imagen con el tama�o de ROI
	//        bordesframe->origin=fondo->origin;
	IplImage * salidabuena = cvCreateImage( cvGetSize(image), 8, 1 );
	//        salidabuena->origin=image->origin;


	//temporales para hacer perspectiva
	IplImage * tempwarp1 = cvCreateImage( cvGetSize(image), 8, 3 );
	//        tempwarp1->origin=image->origin;
	IplImage * tempwarp2 = cvCreateImage( cvGetSize(image), 8, 1 );
	//        tempwarp2->origin=image->origin;

	//temporal para hacer condici�n de actualizaci�n de fondo
	IplImage * imanterior = cvCreateImage( cvGetSize(image), 8, 3 ); //para estimar la cantidad de movimiento
	//        imanterior->origin=image->origin;
	IplImage * restaim = cvCreateImage( cvGetSize(image), 8, 3 );
	//        restaim->origin=image->origin;
	//cvCopy(image,imanterior);
	cvZero(imanterior);
	/*para crear video compuesto de 4 imagenes*/
	//r1r2   -> video salida bordes
	//r3r4   -> FG    diff de bordes
	IplImage * agrabar = cvCreateImage( cvSize(2*image->width , 2*image->height), 8, 3 );
	//        agrabar->origin=image->origin;
	CvRect r1={0,image->height,image->width,image->height};
	CvRect r2={image->width,image->height,image->width,image->height};
	CvRect r3={0,0,image->width,image->height};
	CvRect r4={image->width,0,image->width,image->height};
	//        restaim->origin=image->origin;
#if quierograbar

	double cuadrosporsegundo=-1;
	if(esdecamara)
	cuadrosporsegundo=framespersec(capture);
	else
	cuadrosporsegundo=cvGetCaptureProperty( capture, CV_CAP_PROP_FPS );
	printf("....tenemos %f cuadros por segundo\n",cuadrosporsegundo);
        creavideo = cvCreateVideoWriter("Salida Algoritmo.avi",-1, cuadrosporsegundo, cvSize(2*image->width , 2*image->height));//TODAS LAS IMAGENES DEBEN TENER EL MISMO TAMA�O
	printf("tenemos %f cuadros por segundo\n",cuadrosporsegundo);
#endif
#if mascaradeinteres
	cvNamedWindow("la mascara",1);
	IplImage * mascarapersp = cvCreateImage( cvGetSize(image), 8, 1 );
	//        mascarapersp->origin=image->origin;
	IplImage * temporalmascarapersp = cvCreateImage( cvGetSize(image), 8, 1 );
	//        temporalmascarapersp->origin=image->origin;
	cvSet(temporalmascarapersp,cvScalarAll(255));
	cvWarpPerspective(temporalmascarapersp, mascarapersp, g_c, CV_INTER_CUBIC+8+16,cvScalar(0,0,0));
	cvShowImage("la mascara",mascarapersp);
	//cvWaitKey(0);
	cvDestroyWindow("la mascara");
#endif
	cvSetImageROI(agrabar,r4);    //r1r2   -> video salida bordes
	cvSetImageCOI( agrabar, 1 );
	cvCopy(bordesfondo_inv,agrabar);    //r3r4   -> FG    diff de bordes
	cvSetImageCOI( agrabar, 2 );
	cvCopy(bordesfondo_inv,agrabar);    //r3r4   -> FG    diff de bordes
	cvSetImageCOI( agrabar, 3 );
	cvCopy(bordesfondo_inv,agrabar);    //r3r4   -> FG    diff de bordes
	cvResetImageROI(agrabar);

#if actfondosinmov
	IplImage * casifondo = cvCreateImage(cvSize(image->width ,image->height), 8, 1 );
	//        casifondo->origin=image->origin;
#endif                                                 /*SUPER FOR: Recorre todos los cuadros del video*/
	for(int ijk=0;ijk<Ncuadros;ijk++)                  /*SUPER FOR: Recorre todos los cuadros del video*/
	{//Fin del procesamiento del video                 /*SUPER FOR: Recorre todos los cuadros del video*/
		if(image == NULL)break;
		if(!esdecamara)
		cvSetCaptureProperty( capture, CV_CAP_PROP_POS_FRAMES, ijk);
		imagensinroi = cvQueryFrame( capture );
		cvSetImageROI(imagensinroi,regideinte)  ;//Selecciona una regi�n de inter�s
		//                image->origin=imagensinroi->origin;
		cvCopy(imagensinroi,imagensinpersp);
		//cvWarpPerspective(imagensinpersp, image, g_c, CV_INTER_CUBIC,cvScalar(255,100,255));
		cvCopy(imagensinpersp, image);
		//BordesMorfologia(image,bordesframe,0);//1 para fondo 0 para primer plano
		cvCvtColor( image, bordesframe, CV_RGB2GRAY);
		cvCanny( bordesframe, bordesframe, 80,40, 3 );
#if mascaradeinteres
		cvAnd(mascarapersp,bordesframe,bordesframe,0);
#endif


		//cvShowImage("Video",image);
		//		cvShowImage("Bordes Imagen",bordesframe);
		cvAnd(bordesfondo_inv,bordesframe,diferbord,0);
#if actfondosinmov
		cvThreshold(diferbord,casifondo,1,1,CV_THRESH_BINARY);//los valores de umbral de 50 y de cantidad de movimiento en 0.005 fueron hallados mediante experimentaci�n
		double count2 = cvNorm( casifondo, 0, CV_L1, 0 ); // acumula los unos en la silueta
#endif

		cvAbsDiff(image, imanterior,restaim);
		cvThreshold(restaim,restaim,50,1,CV_THRESH_BINARY);//los valores de umbral de 50 y de cantidad de movimiento en 0.005 fueron hallados mediante experimentaci�n
		double count = cvNorm( restaim, 0, CV_L1, 0 ); // acumula los unos en la silueta
		if(( count > (restaim->width*image->height * 0.005)) ||(count2 <((casifondo->width*casifondo->height * 0.002))) )//si el movimiento total es mayor al 0.05% de el tama�o del la imagen , guarde fondo
		{
#if actfondosinmov
			//printf("Entr� a actualizar ");

#endif
			cvUpdateBGStatModel( imagensinpersp, bgModel );;//acumula la imagen BGStat model solo cuando hay movimiento
		}
#if actfondosinmov
		//printf("count %f de %f, count2 %f de %f\n",count,(restaim->width*image->height * 0.005),count2,((casifondo->width*casifondo->height * 0.002)));

#endif
		//cvUpdateBGStatModel( image, bgModel );//para que siempre actualize
		cvCopy(bgModel->foreground,tempFG);
#if mascaradeinteres
		cvAnd(mascarapersp,tempFG,tempFG,0);
#endif

		//		cvShowImage("FG",tempFG);
		/*para crear video compuesto de 4 imagenes*/
		cvSetImageROI(agrabar,r3); //r1r2   -> video salida bordes
		cvSetImageCOI( agrabar, 3 );
		cvCopy(tempFG,agrabar);    //r3r4   -> FG    diff de bordes
		cvResetImageROI(agrabar);
		cvDilate(diferbord,bordesframe,Kernel,1);
		cvErode(bordesframe,diferbord,Kernel,1);
		cvDilate(diferbord,bordesframe,Kernel2,1);
		cvErode(bordesframe,diferbord,Kernel2,1);

		//cvErode(diferbord,diferbord,Kernel,1);
		//cvDilate(diferbord,diferbord,Kernel2,1);


		cvOr(tempFG,diferbord,salidabuena);
		//cvCopy(tempFG,salidabuena);//sin bordes
		//        cvShowImage("Diferencia de Bordes",diferbord);
		/*para crear video compuesto de 4 imagenes*/
		cvSetImageROI(agrabar,r3);    //r1r2   -> video salida bordes
		cvSetImageCOI( agrabar, 2 );
		cvCopy(diferbord,agrabar);    //r3r4   -> FG    diff de bordes
		cvResetImageROI(agrabar);
		/*se pretende hacer una prueba de corregir perspectiva
		despu�s de hacer todo el algoritmo, y antes de contar y seguir*/
		cvCopy(image,tempwarp1);
		cvWarpPerspective(tempwarp1, image, g_c, CV_INTER_CUBIC,cvScalar(255,255,255));
		cvCopy(salidabuena,tempwarp2);
		cvWarpPerspective(tempwarp2, salidabuena, g_c, CV_INTER_CUBIC,cvScalar(255,255,255));


		//		cvShowImage("salida con bordes", salidabuena);
		/*para crear video compuesto de 4 imagenes*/
		cvSetImageROI(agrabar,r2);      //r1r2   -> video salida bordes
		cvSetImageCOI( agrabar, 1 );
		cvCopy(salidabuena,agrabar);    //r3r4   -> FG    diff de bordes
		cvResetImageROI(agrabar);
		/**********************************************************************************/
		//desde ac� empieza el algoritmo de seguimiento, en la imagen salidabuena     :):):):):):):):):):):):):):):):):):):):):):):)
		/**********************************************************************************/

		cvFindContours( salidabuena, storage, &contour, sizeof(CvContour), CV_RETR_EXTERNAL);//encuentra los contornos mas externos
		/*INICIO ALGORITMO DE union de blobs traviesos*/
		cvClearSeq( cnt_list );
		for(;contour;contour=contour->h_next)//coorra todos los contornos
		{
			perimeter = cvArcLength (contour,CV_WHOLE_SEQ,1);
			double aria=fabs(cvContourArea( contour,CV_WHOLE_SEQ ));
			if(perimeter>Maxperimeter && aria>MAXAREA)
			{
				cvSeqPush( cnt_list, &contour);//meta los contornos en cnt_list para poderlos recorrer mas f�cil
				//los contornos que vamos a trabajar ya estan filtrados por tama�o
			}
		}


		claster_num = cvSeqPartition( cnt_list, stgclaster, &clasters, CompareContorno, NULL );//separa los contornos con la funcion FUNCION CompareContorno
		//		printf( "paso por ac�...\n" );
		int numcontornos=0;
		cvClearSeq( contornos_nuevos );//borra la secuencia de rectangulos para uso futuro
		for(claster_cur=0;claster_cur<claster_num;++claster_cur)
		{/*recorra por clusters*/
#if momentodelblob
			double      M00,X,Y,XX,YY; /* image moments */
			CvMoments   m;
			CvMat       mat;
#endif
			int         cnt_cur;
			CvRect      rect_res = cvRect(-1,-1,-1,-1);
			caracteristica_contorno contorno_temporal;
			for(cnt_cur=0;cnt_cur<clasters->total;++cnt_cur)
			{/*recorre por contornos ya filtrados, y compara si es del mismo cluster */
				CvRect  rect;
				CvSeq*  cnt;
				int k = *(int*)cvGetSeqElem( clasters, cnt_cur );
				if(k!=claster_cur) continue;
				cnt = *(CvSeq**)cvGetSeqElem( cnt_list, cnt_cur );
				rect = ((CvContour*)cnt)->rect;//el contorno ya tiene un bounding rect asi que siendo tan diestros  :'( en OpenCV lo hacemos as� :)
				numcontornos++;
				if(rect_res.height<0)
				{
					rect_res = rect;
				}
				else
				{/* une los rectangulos con el muemo cluster */
					int x0,x1,y0,y1;
					x0 = MIN(rect_res.x,rect.x);
					y0 = MIN(rect_res.y,rect.y);
					x1 = MAX(rect_res.x+rect_res.width,rect.x+rect.width);
					y1 = MAX(rect_res.y+rect_res.height,rect.y+rect.height);
					rect_res.x = x0;
					rect_res.y = y0;
					rect_res.width = x1-x0;
					rect_res.height = y1-y0;
				}
			}/*recorre por contornos ya filtrados, y compara si es del mismo cluster */
			contorno_temporal.rectangulo=rect_res;//hay que ponerlo si se quiere dejar el c�digo modular con #if
			/*ac� dividimos los clusters de m�s de el tama�o de la via en clusters mas peques*/
#if dividaclustergrande //este #if incluye una partici�n para los diferentes casos.

			if (cuentedeabajoaarriba ||cuentedearribaaabajo)
			{
				if(rect_res.width > ((float)salidabuena->width/((float)numero_de_carriles))*1.3)  //es width para cuando el blob sube o baja, si va para los lados no sirve hay que cambiarlo
				{//si el ancho del rect�ngulo es mayor a el 130% del ancho del carril es por que hay cosas unidas o un carro MUY grande  ;D !!!


					int relacionancho = aproxcarril((float)rect_res.width/((float)salidabuena->width/((float)numero_de_carriles)));
					//printf("Ratio %f \n",(float)rect_res.width/((float)salidabuena->width/(float)(numero_de_carriles)));
					//printf("Ancho de blob %d y relacion %d con ancho de frame %d # de carril %d\n",rect_res.width,relacionancho,salidabuena->width,(numero_de_carriles));
					for(int ji =0;ji<relacionancho;ji++){

						CvRect Particion =cvRect(rect_res.x+(int)((ji/(float)relacionancho)*(float)rect_res.width),rect_res.y,(int)((float)(1/(float)relacionancho)*(float)rect_res.width),rect_res.height);
						//printf("CvRect en x %d, y %d con w %d, h %d \n",Particion.x,Particion.y,Particion.width,Particion.height);
						cvMoments(cvGetSubRect(salidabuena,&mat,Particion), &m, 1 );
						M00 = cvGetSpatialMoment( &m, 0, 0 );
						//printf("Momento 00 %f",M00);
						if(M00 <= 0 ) continue;
						X = cvGetSpatialMoment( &m, 1, 0 )/M00;
						Y = cvGetSpatialMoment( &m, 0, 1 )/M00;
						XX = (cvGetSpatialMoment( &m, 2, 0 )/M00) - X*X;
						YY = (cvGetSpatialMoment( &m, 0, 2 )/M00) - Y*Y;
						contorno_temporal.rectangulo=cvRect(Particion.x+cvRound(X-.50f*4*sqrt(XX)),Particion.y+cvRound(Y-0.50f*4*sqrt(YY)),cvRound(4*sqrt(XX)),cvRound(4*sqrt(YY)));
						//printf("CvRect en x %d, y %d con w %d, h %d \n",contorno_temporal.rectangulo.x,contorno_temporal.rectangulo.y,contorno_temporal.rectangulo.width,contorno_temporal.rectangulo.height);
						cvSeqPush(contornos_nuevos,&contorno_temporal);//entra por delante
					}
					continue;
					//
				}
				else
				{
#if momentodelblob
					/*ac� uniriamos por momentos Y tambien podria ir lo de partici�n de blobs ENORMES dos veh�culos � m�s "no motos :)" */
					if(rect_res.height < 1 || rect_res.width < 1)
					{
						X = 0;
						Y = 0;
						XX = 0;
						YY = 0;
					}
					else
					{
						cvMoments(cvGetSubRect(salidabuena,&mat,rect_res), &m, 1 );
						M00 = cvGetSpatialMoment( &m, 0, 0 );
						if(M00 <= 0 ) continue;
						X = cvGetSpatialMoment( &m, 1, 0 )/M00;
						Y = cvGetSpatialMoment( &m, 0, 1 )/M00;
						XX = (cvGetSpatialMoment( &m, 2, 0 )/M00) - X*X;
						YY = (cvGetSpatialMoment( &m, 0, 2 )/M00) - Y*Y;
					}
					contorno_temporal.rectangulo=cvRect(rect_res.x+cvRound(X-.50f*4*sqrt(XX)),rect_res.y+cvRound(Y-0.50f*4*sqrt(YY)),cvRound(4*sqrt(XX)),cvRound(4*sqrt(YY)));
					//contorno_temporal.rectangulo=cvRect(rect_res.x,rect_res.y,cvRound(4*sqrt(XX)),cvRound(4*sqrt(YY)));
#endif


#if dividaclustergrande

				}
#endif
			}

			if (cuentedeizquierdaaderecha ||cuentedederechaaizquierda)
			{

				if(rect_res.height > ((float)salidabuena->height/((float)numero_de_carriles))*1.35)  //es width para cuando el blob sube o baja, si va para los lados no sirve hay que cambiarlo
				{//si el ancho del rect�ngulo es mayor a el 130% del ancho del carril es por que hay cosas unidas o un carro MUY grande  ;D !!!


					int relacionancho = aproxcarril((float)rect_res.height/((float)salidabuena->height/((float)numero_de_carriles)));
					//printf("Ratio %f \n",(float)rect_res.height/((float)salidabuena->height/(float)(numero_de_carriles)));
					//printf("Alto de blob %d y relacion %d con alto de frame %d # de carril %d\n",rect_res.height,relacionancho,salidabuena->height,(numero_de_carriles));
					for(int ji =0;ji<relacionancho;ji++){

						CvRect Particion =cvRect(rect_res.x,rect_res.y +(int)((ji/(float)relacionancho)*(float)rect_res.height),rect_res.width,(int)((float)(1/(float)relacionancho)*(float)rect_res.height));
						//printf("CvRect en x %d, y %d con w %d, h %d \n",Particion.x,Particion.y,Particion.width,Particion.height);
						cvMoments(cvGetSubRect(salidabuena,&mat,Particion), &m, 1 );
						M00 = cvGetSpatialMoment( &m, 0, 0 );
						//printf("Momento 00 %f",M00);
						if(M00 <= 0 ) continue;
						X = cvGetSpatialMoment( &m, 1, 0 )/M00;
						Y = cvGetSpatialMoment( &m, 0, 1 )/M00;
						XX = (cvGetSpatialMoment( &m, 2, 0 )/M00) - X*X;
						YY = (cvGetSpatialMoment( &m, 0, 2 )/M00) - Y*Y;
						contorno_temporal.rectangulo=cvRect(Particion.x+cvRound(X-.50f*4*sqrt(XX)),Particion.y+cvRound(Y-0.50f*4*sqrt(YY)),cvRound(4*sqrt(XX)),cvRound(4*sqrt(YY)));
						//printf("CvRect en x %d, y %d con w %d, h %d \n",contorno_temporal.rectangulo.x,contorno_temporal.rectangulo.y,contorno_temporal.rectangulo.width,contorno_temporal.rectangulo.height);
						cvSeqPush(contornos_nuevos,&contorno_temporal);//entra por delante
					}
					continue;
					//
				}
				else
				{
#if momentodelblob
					/*ac� uniriamos por momentos Y tambien podria ir lo de partici�n de blobs ENORMES dos veh�culos � m�s "no motos :)" */
					if(rect_res.height < 1 || rect_res.width < 1)
					{
						X = 0;
						Y = 0;
						XX = 0;
						YY = 0;
					}
					else
					{
						cvMoments(cvGetSubRect(salidabuena,&mat,rect_res), &m, 1 );
						M00 = cvGetSpatialMoment( &m, 0, 0 );
						if(M00 <= 0 ) continue;
						X = cvGetSpatialMoment( &m, 1, 0 )/M00;
						Y = cvGetSpatialMoment( &m, 0, 1 )/M00;
						XX = (cvGetSpatialMoment( &m, 2, 0 )/M00) - X*X;
						YY = (cvGetSpatialMoment( &m, 0, 2 )/M00) - Y*Y;
					}
					contorno_temporal.rectangulo=cvRect(rect_res.x+cvRound(X-.50f*4*sqrt(XX)),rect_res.y+cvRound(Y-0.50f*4*sqrt(YY)),cvRound(4*sqrt(XX)),cvRound(4*sqrt(YY)));
					//contorno_temporal.rectangulo=cvRect(rect_res.x,rect_res.y,cvRound(4*sqrt(XX)),cvRound(4*sqrt(YY)));
#endif


#if dividaclustergrande

				}
#endif
			}

#endif //Endif de dividaclustergrande!!!



			/*espacio para asignar el claster a la estructura*/
			cvSeqPush(contornos_nuevos,&contorno_temporal);//entra por delante
			//            printf( "a�adi bien en ...(%d,%d) ...\n" ,contorno_temporal.rectangulo.x,contorno_temporal.rectangulo.y);
		}/*fin del recorrido por clusters*/






		/*solo sirve para debug, no tiene funci�n en el algoritmo, muestra las caracteristicas del cluster */
		//		printf( "numero de clasters que encontre %d\n" ,contornos_nuevos->total);
		//		for(int h = 0; h < contornos_nuevos->total; h++ )//hasta que encuentre el primer contorno entra a este for
		//	       {
		//               caracteristica_contorno* pcontorno = (caracteristica_contorno*)cvGetSeqElem(contornos_nuevos, h );
		//               printf( "asigne rectangulo en el punto (%d,%d)\n" ,pcontorno->rectangulo.x,pcontorno->rectangulo.y);
		//           }


		if(fg)
		{//primera vez que se corre asigna TODOS los contornos a un blob nuevo
			for(int h = 0; h < contornos_nuevos->total; h++  )//hasta que encuentre el primer contorno entra a este for
			{
				//perimeter = cvArcLength (cntmp,CV_WHOLE_SEQ,1);
				//double aria=fabs(cvContourArea( cntmp,CV_WHOLE_SEQ ));
				//printf( "el area es %f \n",aria);
				//if(perimeter>Maxperimeter && aria>MAXAREA)
				//{
				caracteristica_contorno* pcontorno = (caracteristica_contorno*)cvGetSeqElem(contornos_nuevos, h );
				//bndRect = cvBoundingRect(cntmp, 0);//Retorna la aproximaci�n en rect�ngulos de cada uno de los contornos.
				bndRect=pcontorno->rectangulo;
				//                    printf( "asigne rectangulo en el punto (%d,%d)\n" ,pcontorno->rectangulo.x,pcontorno->rectangulo.y);
				//bndRect = cvBoundingRect(cntmp, 0);//Retorna la aproximaci�n en rect�ngulos de cada uno de los contornos.
				sq1.x = (int)bndRect.x;
				sq1.y = (int)bndRect.y;
				sq2.x = (int)bndRect.x + (int)bndRect.width;
				sq2.y = (int)bndRect.y + (int)bndRect.height;
				cvRectangle(image, sq1, sq2, CV_RGB(255,0,255), 1);
				tpv.x=(int)sq1.x + (int)(bndRect.width  /2);
				tpv.y=(int)sq1.y + (int)(bndRect.height /2);
				blob nublob = nuevoblob(aidi,tpv,tpv,0,1,0,cvSize(bndRect.width,bndRect.height),1);
				cvSeqPush( blobs, &nublob );//por delante
				aidi++;
				fg=0;
				//}
			}
		}//primera vez que se corre



		/*desde ac�*/
		if(numcontornos != 0)
		{//si no tengo contornos nuevos no continue
			i=0;
			j=0;//variables que recorren la secuencia
			int h=0;
			//cntmp = contour;
			for(h = 0; h < contornos_nuevos->total; h++  )//int h = 0; h < contornos_nuevos->total; h++
			{//recorro todos los contornos y los comparo uno a uno con los blobs y guardo el respectivo error uno a uno
				//perimeter = cvArcLength (cntmp,CV_WHOLE_SEQ,1);
				//double aria=fabs(cvContourArea( cntmp,CV_WHOLE_SEQ ));
				//if(perimeter>Maxperimeter && aria>MAXAREA)
				//{//filtro de perimetros
				caracteristica_contorno* pcontorno = (caracteristica_contorno*)cvGetSeqElem(contornos_nuevos, h );
				//bndRect = cvBoundingRect(cntmp, 0);//Retorna la aproximaci�n en rect�ngulos de cada uno de los contornos.
				bndRect=pcontorno->rectangulo;
				sq1.x = bndRect.x;
				sq1.y = bndRect.y;
				sq2.x = bndRect.x + (bndRect.width);
				sq2.y = bndRect.y + (bndRect.height);
				cvRectangle(image, sq1, sq2, CV_RGB(255,0,255), 1);

				pcont.x = (int)bndRect.x + (int)(bndRect.width/2);
				pcont.y = (int)bndRect.y + (int)(bndRect.height/2);//en sq2 quedo el punto central del contorno
				for(i = 0; i < blobs->total; i++ )//i recorre los blobs i = 0; i < blobs->total; i++
				{
					blob* pblob = (blob*)cvGetSeqElem(blobs, i );// corre de 0 a blobs->total-1
					tpt = pblob->ptd;//en tpt queda el punto central de cada blob          NOTA CAMBIE A pta cuando lo guarde ac�
					regi[i][j].pt=pcont;//aqui guardamos el punto presente
					regi[i][j].dist = error(tpt,pcont);//distancia de los puntos
					regi[i][j].id[0] = i;//blob antiguo
					regi[i][j].id[1] = j;//contorno nuevo
					//printf( "Contorno %d con Tama�o %d,%d ubicado en %d,%d NO INT\n",j,bndRect.width,bndRect.height,pblob->ptd.x,pblob->ptd.y);
					regi[i][j].tam = cvSize(bndRect.width,bndRect.height);//tama�o del contorno encontrado
				}//i recorre los blobs
				j++;//recorre los contornos
				//}//filtro de perimetros
			}//recorri todos los contornos y los comparo uno a uno con los blobs y guardo el respectivo error uno a uno
			/*indices de caracteristicas nuevas y viejas*/
			j--;
			i--;
			if(i<0)//bug de indices solucionado
			{
				i=0;
			}
			if(j<0)
			{
				j=0;
			}
			/**/
			int ti=i;
			int tj=j;
			//		printf( "debe correr (i,j) = (%d , %d)\n",i,j);
			// lo que falta es recorrer los blobs, y buscar cual es de la matriz la que tiene el menor error
			registro temp;
			temp.dist=10000000.0f;//un n�mero muy grande
			temp.id[0]=0;
			temp.id[1]=0;
			// for que pome todas las banderas de marcado en 1
			for(int l=0;l<TAM;l++)
			{
				for(int m=0;m<TAM;m++)
				{
					regi[l][m].flag = 1;  //marca todos como no asignado
				}
			}
			//for que borra las banderas de los blobs para ver cual encontro y no debe ser borrado, 1 borrar 0 no borrar
			for(int p = 0; p < blobs->total; p++ )//p recorre los blobs y resetea las banderas de los blobs
			{
				blob* tpblob = (blob*)cvGetSeqElem(blobs, p );
				tpblob->band = 1;
			}
			for(int k=MIN(i,j) ; k>=0 ; k--)
			{//recorremos la menor de las cantidades de blobs y contornos los contornos nuevos ser�n a�adidos luego
				temp.dist = 1000000;//reset de las variables
				temp.id[0]=0;
				temp.id[1]=0;
				ti=i;
				tj=j;
				for(ti=0;ti<=i;ti++)//recorrer matriz por filas "blobs"
				{
					for(tj=0;tj<=j;tj++)//recorremos la matriz por columnas "contornos nuevos"
					{
						if(regi[ti][tj].dist<temp.dist && regi[ti][tj].flag == 1)// si el error es menor a la �ltima menor encontrada Y no esta marcado como ya asignado
						{
							//marquelo para que no lo encuentre de nuevo "despu�s de vuelve a borrar asi que esto sobra"
							temp=regi[ti][tj];//asignelo
						}
						//ojo que nos toca asegurar que tanto ese contorno como ese blob no lo vuelva a asignar
						//osea que hay que marcar TODA la columna
					}
				}//termine de recorrer la matriz ya esta el menor sin marcar en temp

				//printf( "va a pedir el blob %d\n",temp.id[0]);
				blob* pblob = (blob*)cvGetSeqElem(blobs, temp.id[0] );//traer el blob i "el de menor error con el contorno que se ha encontrado" y evalue:
				if(temp.dist<MAX_DISTANCIA_DE_ASIGNACION)
				{//solo asigna cuando la distancia es menor a MAX_DISTANCIA_DE_ASIGNACION si no deja el blob para ser asignado como nuevo
					if(pblob->band == 1)//si el blob no ha sido modificado, o la distancia que voy a poner es grandota, no lo asigne
					{
						//if((pblob->bandnuevo==1))
						if (cuentedeabajoaarriba)
						{
							//{  //Filtro para asignaciones entre blob y contornos erroneas y filtrado de blobs peque�os
							if((temp.dist<MAX(pblob->tamano.height,pblob->tamano.width)) && (pblob->ptd.y>=(cvGetSize(image).height/2) && temp.pt.y<(cvGetSize(image).height/2))&& (pblob->bandcontado==1))
							{
								contdef++;//cuente un vehiculo mas
								pblob->bandcontado=0;
							}
						}//#endif
						if (cuentedearribaaabajo)
						{
							//{  //Filtro para asignaciones entre blob y contornos erroneas y filtrado de blobs peque�os
							if((temp.dist<MAX(pblob->tamano.height,pblob->tamano.width)) && (pblob->ptd.y<=(cvGetSize(image).height/2) && temp.pt.y>(cvGetSize(image).height/2))&& (pblob->bandcontado==1))
							{
								contdef++;//cuente un vehiculo mas
								pblob->bandcontado=0;
							}
						}//#endif
						if (cuentedeizquierdaaderecha)
						{
							//{  //Filtro para asignaciones entre blob y contornos erroneas y filtrado de blobs peque�os
							if((temp.dist<MAX(pblob->tamano.height,pblob->tamano.width)) && (pblob->ptd.x<=(cvGetSize(image).width/2) && temp.pt.x>(cvGetSize(image).width/2))&& (pblob->bandcontado==1))
							{
								contdef++;//cuente un vehiculo mas
								pblob->bandcontado=0;
							}
						}//#endif
						if (cuentedederechaaizquierda)
						{
							//{  //Filtro para asignaciones entre blob y contornos erroneas y filtrado de blobs peque�os
							if((temp.dist<MAX(pblob->tamano.height,pblob->tamano.width)) && (pblob->ptd.x>=(cvGetSize(image).width/2) && temp.pt.x<(cvGetSize(image).width/2))&& (pblob->bandcontado==1))
							{
								contdef++;//cuente un vehiculo mas
								pblob->bandcontado=0;
							}
						}//#endif
						/*como termine de recorrer la matriz, y saque el menor sin marcar, lo asigno*/
						/*si entro ac� es por que ya estaba creado y lo estoy siguiendo, osea ya tiene el ID y lo conserva*/
						sq1.x=temp.pt.x;//actualizar la posici�n
						sq1.y=temp.pt.y;
						pblob->band=0;//el blob que queda sin marcar se elimina
						pblob->pta = pblob->ptd;//asigno el punto anterior
						pblob->ptd=sq1;//se actualiza en el punto mas reciente, el punto pasado pta queda para estudio previo
						//pblob->bandcontado=0;
						for(int u=0;u<TAM;u++)/*for para marcar TODA la fila de blobs i y la columna de contornos j*/
						{
							regi[u][temp.id[1]].flag=0;//errores de todos los blobs a un contorno
						}//en este momento esta encontrado el menor sin marcar listo para ser asignado
						//}//Filtro para asignaciones entre blob y contornos erroneas y filtrado de blobs peque�os
					}
					else
					{//si fue modificado ya es por que encontramos una error menor antes y no lo quiero asignar de nuevo y busque otra vez
						regi[temp.id[0]][temp.id[1]].flag=0;
						k++;//para que busque otra vez
					}//si fue modificado ya es por que encontramos una error menor antes y no lo quiero asignar de nuevo y busque otra vez
				}//solo asigna cuando la distancia es menor a MAX_DISTANCIA_DE_ASIGNACION si no deja el blob para ser asignado como nuevo
			}//termina de hacer seguimientos
			/*BORRAR BLOBS PERDIDOS*/
			int numblob=blobs->total;
			for(int p = 0;p < numblob ;p++)//p recorre los blobs para borrar lo que no est� marcado con band=1
			{
				blob* tblob = (blob*)cvGetSeqElem(blobs, p );//trae la informaci�n de blob i
				if(tblob->band)//si la bandera esta en uno lo borra
				{
					cvSeqRemove( blobs, p );//borra el blob x de la secuencia
					if (p!=0)//mientras no sea el �ltimo blob que queda
					{
						p--;//si no se pone eso se queda un blob sin revisar
					}
					numblob--;
				}
			}
			ti=i;
			tj=j;
			int pongablob=0;
			/*a�adir nuevos blobs encontrados a la secuencia*/
			for(tj=0;tj<=j;tj++)
			{//recorremos la matriz por columnas "contornos nuevos"
				for(ti=0;ti<=i;ti++)
				{//recorrer matriz por filas "blobs"
					if(regi[ti][tj].flag)
					{//si el contorno no esta asignado
						pongablob=1;//hay que sacer la menor de las distancias ac� tambien para que no tenga errores al asignar!!!/*OJO OJO OJO*/
						temp=regi[ti][tj];
					}//si el contorno no esta asignado
				}//recorrer matriz por filas "blobs"
				if(pongablob)//si encontre uno sin marcar asigne el temporal
				{//pongablob
					sq2.x = temp.pt.x;
					sq2.y = temp.pt.y;
					blob nublob = nuevoblob(aidi,sq2,sq2,0,1,0,regi[i][j].tam,1);
					cvSeqPush( blobs, &nublob );//por delante
					aidi++;
					pongablob=0;
				}//pongablob
			}//recorremos la matriz por columnas "contornos nuevos"
			for(int p = 0; p < blobs->total; p++ )
                        {//poner numeritos en el cuadro con el blob
				blob* pblob = (blob*)cvGetSeqElem(blobs, p );
				cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 0.5, 0.5, 0, 2);
                                sprintf(numeros,"%d",(int)pblob->Id);
                                cvPutText(image,numeros, pblob->ptd, &font, cvScalar(255, 100,255));
				cvLine( image, pblob->pta, pblob->ptd, cvScalar(130, 255,68),1,8,0);
			}//poner numeritos en el cuadro con el blob
		}//si no tengo contornos nuevos no continue
		/*hasta ac�*/
		cvInitFont(&defin, CV_FONT_HERSHEY_SCRIPT_SIMPLEX, 0.7, 0.7, 0, 3);
                sprintf(conteodef,"%d",contdef);
                cvPutText(image, conteodef, cvPoint((int)10,(int)l-1), &defin, cvScalar(255, 255,0));

		if (cuentedeabajoaarriba)
		cvLine( image, cvPoint(0,(cvGetSize(image).height/2)), cvPoint(cvGetSize(image).width,cvGetSize(image).height/2), cvScalar(128, 128,255),1,8,0);//dibuja la linea
		//#endif
		if (cuentedearribaaabajo)
		cvLine( image, cvPoint(0,(cvGetSize(image).height/2)), cvPoint(cvGetSize(image).width,cvGetSize(image).height/2), cvScalar(128, 128,255),1,8,0);//dibuja la linea
		//#endif
		if (cuentedeizquierdaaderecha || cuentedederechaaizquierda)
		cvLine( image, cvPoint((cvGetSize(image).width/2),0), cvPoint(cvGetSize(image).width/2,cvGetSize(image).height), cvScalar(128, 128,255),1,8,0);//dibuja la linea
		//#endif

		cvCopy(image,tempwarp1);
		cvWarpPerspective(tempwarp1, image, g_c, CV_INTER_CUBIC+8+16,cvScalar(0,0,0));

#if mascaradeinteres
		cvNot(mascarapersp,temporalmascarapersp);
		cvCopy(imagensinpersp,image,temporalmascarapersp);

#endif

		//cvShowImage( "Video", image );//muestra el cuadro que se capturo en la ventana captura
		/*para crear video compuesto de 4 imagenes*/
		cvSetImageROI(agrabar,r1);//r1r2
		cvSetImageCOI( agrabar, 0 );
		cvCopy(image,agrabar);    //r3r4
		cvResetImageROI(agrabar);
#if quierograbar
		cvWriteFrame(creavideo, agrabar);
#endif
		cvShowImage( "Video", agrabar );
		cvClearMemStorage(storage);
		cvCopy(imagensinpersp,imanterior);// posible bug  }:)
                char key_pressed=0;
                key_pressed = (char)(cvWaitKey(3)&0xff);
		if (ijk>=Ncuadros+1 ||ijk< 0)break;
		if ((key_pressed & 255) == 27 ) break;//sale en Esc
	}//video
	//cvWaitKey(0);
#if quierograbar
	cvReleaseVideoWriter(&creavideo);
#endif
	cvReleaseBGStatModel( &bgModel );
	cvReleaseCapture(&capture);
	Free2DMat<registro>(regi);


	return 0;

	/*FIN OPENCV*/
}


/*

        ...................................................... .   .
       .. ......................O,,............................. ..  .  ..  .
  .    .        .     ..   .   .78DOI7..= ...   .  .  . .  .  .      .      .
  .             . .   ..   .    .?887$8Z88O8$:+I. .........   ......... .. .. ..
  . . ..        ................ ..Z88D:88=I8$?88888I7Z,.,~   .      .      .
           .    ................  ..,78D88+O8I+88.I88Z88888888$,..
           .    ................    .. =$8888DZ8D:$8I.88$O8888$$88?.
..... ... .. ..............................:+OO88O88OI88:8D,$88Z8:7OZ~
           . .  ................      . .   . . .,:~?88888D~8$:O88O$O?Z+..... .
           .    ................                 .......=Z888$+8+7$?8$Z8O
           .    ........ .......                        ....~Z88+8OZ8ID?+8=.
   =OOO$7=,.    ......,:, ......        ....?I?$7I,,,.      ...~Z8=78$8OO?ZO...
  :D8888888D8$?=,:?OD8O8D8~.....  ,==+?7887OO~$8IZDI7888Z?I=. . .$8O88I:DIZ8~..
   .$IZ8888888888OOO8$..788888ZIZO8888ZO8Z78OZ88I8DOO8~788?:O8OZ:I88888Z88=Z~..
    . .+:888888888888888888888888O~8888,+8=~8Z.$D:+8+:87,8888I?OZ+O$:O=O+O?8O..
.       ..=+7IZI88888888888888?88888888~$8788Z788$$88+$8OZD~=8$:Z8$Z8?Z8Z8,ZO
         .........:Z$777?,$888O+D878888$888D8888OZ8888888ODO$88:78?8OZOO:888D.
.     .    ..  . ~Z$$$Z888$=?I=7DZ888887$O~Z8++88IZ88+I8Z+Z8$88888+?8+Z888888:.
         ..$,Z$O8O$$$$O8ZO888O88O~8888888888888888+.OO.I8O,O8,7Z.788O=,~$888D+
      .,IZ888Z$$$Z$$$$$ZO8888888I8888888888888888888888O888888888887+8888=$8D+
  .7888$Z$$$Z7888888888888888888888888888888OOOOO88888D==$888888888:O88888888~.
..:88888888888888888888O$?Z888888888888888888888$?:I88888I~+O888888=78888?78O.
   .~78DDDOOO$$III+=:,=ZOD88888888888888888888DD888Z:O8888DZ:I8888$7.88888IZ..
     .....................:+$8888888888888$+?IIIO888877888888OO8?I88888888=.
        . .. ................. ...........$88888888888:+O88$II?,:888888888D:
.          . .  .................  .  . .I8888888888OZ.. ..... ..+8OO888887.
           . .  ................   .  ...IZ88D888888,  .  . .    ...ID7OO=  ..
..    .    . .  ................   .  . ...+$,7O+88I.. .  . .      ... .. .$77I.
           .    ................               .  .                      ..77DI.
           .    ................                                            ...
  I8888888O..I888:.......:$88O$,   .=O88O+.    .IOM88I,.I88888888O.$8888888O.
.,DMMMMMMMM. OMMM=.....+MMMMMMMM$.ZMMMMMMMMZ. ZMMMDDMMMIOMMMMMMMMN,MMMMMMMMM.
 .DMMM.....  OMMM=....~MMMI...?+.?MMM$..:NMM7.MMM$..,$~ . .$MM8.. .MMMM..
..DMMMDD8$.. OMMM=....ZMM8...... NMMO.. .?MM8.$MMMM8Z...   $MMN.  .MMMMD88$.
 .DMMMMMMM.. OMMM=....8MM$.......MMMI .  =MM8..7MMMMMMD.   $MMN.  .MMMMMMMN.
 .DMMM:::,...OMMM=....7MMM. .....8MM8.. .$MMO. ,. :$MMMN.  $MMN.  .MMMM:::,.
 .DMMM$IIII  OMMM8OO$.,MMMNI:7NM7.MMMM+~ZMMM:.MMMO~=NMMD.  $MMN.  .MMMM7IIII
..DMMMMMMMM, OMMMMMMN..,NMMMMMMM?..DMMMMMMN:..IMMMMMMMN..  $MMN.  .MMMMMMMMM,
 ..+??????~. .=??III:... .~$$?,..   .=$$7:.    .:I$I~...   .?I=.   ,+??????~

                          ...---...
                       ../  / | \  \..
                     ./ /  /  |  \  \ \.
                    /  /   /  |  \   \  \
                   /  /   /   |   \   \  \
                   ^^^^^^^^^^^^^^^^^^^^^^^
                   \          |          /
                    \         |         /
                     \        |        /
                      \       |       /
                       \      |      /
                        \     |     /
                         \    |    /
                          \   |   /
                           \  |  /
                            \ | /(__)
                             \|/ (oo)
                          /---++--\/
                         / |  || ||
                        *  ||-++-||
                           ^^    ^^


           (__)      (__)                                           (__)
           (oo)      (oo)                        _______            (oo)
    /-------\/        \/-------\                / /   ||\            \/------\
   / |     ||          ||     | \         _____/_/____||_\___         ||     |\
  *  ||----||          ||----||  *        )  _          _    \        ||----||*
     ^^    ^^          ^^    ^^           |_/ \________/ \___|        ^^    ^^
____________________________________________\_/________\_/__________________________

    _______
   /______/"=,
  [     | "=, "=,,
  [-----+----"=,* )
  (_---_____---_)/
    (O)     (O)

    ____/   ___
   |_   \__'  _\      (de lado)
   `-(*)----(*)'

       ____
    __/___/______
   |_ \__'   \"_"\      (mas atr�s)
   `())--,())'-,,'

        ____
      _/____]__
     |_v'_]"__"]      (mas )
     `UJ-uJ--uJ

       _____
      i_____i
      ["___"]
      |J---L|      (Por detr�s)



                                 _..-------++._
                             _.-'/ |      _||  \"--._
                       __.--'`._/_\j_____/_||___\    `----.
                  _.--'_____    |          \     _____    /
                _j    /,---.\   |        =o |   /,---.\   |_
               [__]=/ / .-. \\==`===========/==// .-. \\=[__]
                 `-._|\ `-' /|___\_________/___|\ `-' /|_.'
                       `---'                     `---'
MADE BY TESOS INSTRUMENTS
M@-Zi-NGeR  & P@ch0m@n





*/

